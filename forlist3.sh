#!/bin/bash


# Declare an array of string with type
declare  StringArray=("Linux Mint" "Fedora" "Red Hat Linux" "Ubuntu" "Debian")

# Iterate the string arrat using for loop
for val in ${StringArray[@]}
do
	echo $val
done
