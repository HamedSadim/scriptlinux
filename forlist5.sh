#!/bin/bash

LanguageArray=("PHP" "Java" "C#" "C++" "VB.NET" "Python" "Perl")

# print array values in lines
echo "Print every element  in new line"

for val1 in ${LanguageArray[*]}
do
 echo $val1
done

echo ""

# print array values in one line
echo "print all elements in a single line"

for val2 in "${LanguageArray[*]}"
do
	echo $val2
done

