#!/bin/bash

# bash script examples that demos sleep command

SLEEP_TIME="10"

echo "current time: $(date)"
echo "Hi, I'm sleeping for $SLEEP_TIME seconds ..."
sleep $SLEEP_TIME
echo "All done and current time: $(date)"


