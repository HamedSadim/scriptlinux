#!/bin/bash

for file  in $1/*

echo -n "$file: "
ls -ld $file | tr -s " " | cut -d " " -f 3 | tr a-z A-Z
done
