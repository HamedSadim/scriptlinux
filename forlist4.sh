#!/bin/bash

# Declare a string array with type
declare StringArray=("Windows XP" "Windows 10" "Windows ME" "Windows 8.1" "Windows server 2016")

# Read the array values with space
for val in "${StringArray[@]}"
do
	echo $val
done
