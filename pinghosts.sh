#!/bin/bash

#Scrijf een script dat een voor een de hosts in deze file ping
# Usage : ./whilleping.sh host1 host2 host3
# examples: ./whileping.sh www.kdg.be www.cnn.com www.dns.be

while [ ! -z "$1" ] # het zal niet reageren als je geen parameter geeft !
do
	ping -c2 $1 >/dev/null &&  echo "$1 responding ping"|| echo "$1  not responding"
	shift
done
