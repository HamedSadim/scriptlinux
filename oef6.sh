#!/bin/bash

#!/bin/bash

USER=hamed
HOME=/home/hamed

[ `id -u` -eq 0 ] || { echo "run $0 as root" ; exit 1 ; }
find $HOME -user root -exec chown $USER.$USER {} \; -print
