#!/bin/bash

echo
echo "Please chose one of the options below"
echo
echo "a = Display Date and Time"
echo
echo "b = List files and Directory's"
echo
echo "c = List user logged in"
echo
echo "d = Check system uptime"
echo
	read choices

	case $choices in
a) date;;
b) ls;;
c) who;;
d) uptime;;
*) echo invalid choice - bye
	esac

