#!/bin/bash

#while demo
#Controleer of we een parameter hebben, of fout + exit
if ! [ "$1" -eq "$1" ] 2>/dev/null
then
	echo parameter is fout
	echo Geef een seconden teller als parameter
	exit  1
	fi

counter=$1
while [ "$counter" -gt "0" ]
do
	echo $counter
	sleep 1
	counter=$((counter-1))
done
