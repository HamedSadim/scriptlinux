#!/bin/bash

[ -z "$1" ] && { 
echo "usage: $0 [start] | [stop]" exit 1; }

case $1 in
	start) echo "starting service" ;;
	stop) echo "stopping service" ;;
	*) echo "usage: $0 [start[ | [stop]" ;; # in alle andere gevallen
esac
