#!/bin/bash


total=`ls -l linux* | wc -l`

echo "It will take $total second to assign permissions.."
echo
for i in linux.*
do
	echo "Assigning write permissions to $i"
	chmod +x $i
	sleep 1
done
