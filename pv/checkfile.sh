#!/bin/bash

# The script will check for files

FILES="/etc/passwd
/etc/group
/etc/shadow
/etc/nsswitch.conf
/etc/ssh/sshd_config
/etc/fake"

echo
for file in $FILES
do
	if [ ! -e $file ]
	then
	echo "$file do not exits"
	echo
	fi
done
