#!/bin/bash

cd
for DIR in *
do
	CHK=$(grep -c "/home/$DIR" /etc/passwd)
	if [ $CHK -ge 1 ]
	then
	echo "$Dir is assigned to a user"
	else
	echo "$Dir is not assigned to a user"
	fi
done

